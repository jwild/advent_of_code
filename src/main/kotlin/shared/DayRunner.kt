package shared

fun main() {
    advent22.Day25().solve(
        skip1 = false,
        push1 = false,
        skip2 = false,
        push2 = false
    )
}